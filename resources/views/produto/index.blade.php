@extends('layout.master')
@section('content')
    <div class="container-fluid">
        <div class="row">
            @include('layout.navegacao')

            <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
                <div
                    class="d-flex justify-content-between flex-md-nowrap align-items-center border-bottom mb-3 flex-wrap pt-3 pb-2">
                    <h1 class="h2">@yield('title', 'Produtos')</h1>
                </div>



                <div class="table-responsive small">
                    <table class="table-striped table-sm table">
                        <thead>
                            <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Produto</th>
                                <th scope="col">Ações</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>Mouse</td>
                                <td>
                                    <strong>Detalhes </strong><strong>Editar </strong><strong>Excluir</strong>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </main>
        </div>
    </div>
@endsection
