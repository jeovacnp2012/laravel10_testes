@extends('layout.master')


@section('content')
    <div class="container-fluid">
        <div class="row">
            @include('layout.navegacao')

            <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
                <h2>@yield('title', 'Dashboard')</h2>


            </main>
        </div>
    </div>
@endsection
