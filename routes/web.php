<?php

use App\Http\Controllers\Homecontroller;
use App\Http\Controllers\ProdutoController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', [Homecontroller::class, 'index'])->name('home');

Route::prefix('produtos')->group(function () {
    Route::get('/produtos', [ProdutoController::class, 'index'])->name('produto.index');
    //Route::get('/', [ProdutoController::class, 'index'])->name('produto.index');
});
